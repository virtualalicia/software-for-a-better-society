import React from 'react';
import { Navigation } from '../../components/globals/Navigation';
import { Wrapper, Row, H1, CustomWidthWrapper, Subheading, CustomContainer } from '../../components/globals';
import { UN5, SchoolToUni, IndustrySociety, Resources } from './elements';


export default function TDT10() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/teaching">teaching</a>&#47;<a href="/tdt10">tdt10</a></Navigation>
          <CustomContainer>
            <H1>TDT10-Gender and diversity in software development</H1>
            <p>The purpose of this course is to investigate why and how increased understanding of the role of gender and diversity can contribute to better knowledge, processes, and solutions for Software Development.</p>
            <p>
              In order to be admitted to TDT10, students should submit a poster and apply for grant to participate to the
              9th ACM Celebration of Women in Computing: womENcourage 2022 Larnaka, Cyprus, 21-23 September, 2022 <a href="https://womencourage.acm.org/2022/" target="_blank" rel="noopener noreferrer"> conference.</a>
            </p>
            <p>The course will be organized as presentations by the teacher, by the students, by external actors. Monthly meetings will be organized (indicative slot Thursday from 13-15 – start first Thursday of September).</p>
            <p>Papers will be chosen from this list in agreement with the teacher.</p>
            <iframe style={{ width: '100%', maxWidth: '600px', minHeight: '315px', margin: '1rem 0' }} src="https://www.youtube.com/embed/V7hzX9UgWeI" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>

            <Subheading>Syllabus</Subheading>
            {UN5}{SchoolToUni}{IndustrySociety}
            <Subheading>Resources (not syllabus)</Subheading>
            {Resources}
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
