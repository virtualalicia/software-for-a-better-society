import React                   from 'react';
import { Navigation }          from '../../components/globals/Navigation';
import { ActiveProjectsTable } from './elements';
import { Wrapper, Row, H1, CustomWidthWrapper, CustomContainer } from '../../components/globals';

export default function ActiveProjects() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/projects">projects</a>&#47;<a href="/active-projects">active</a></Navigation>
          <CustomContainer>
            <H1>Active Projects</H1>
            <ActiveProjectsTable cellPadding={10} cellSpacing={10}>
              <tbody>  
                <tr>
                  <td><a href="https://www.ntnu.edu/web/idi/senobr" target="_blank" rel="noopener noreferrer">SENOBR</a></td>
                  <td>Software Engineering Practices and Experiences Exchange between Norway and Brazil</td>
                  <td>2021 - 2024</td>
                  <td><a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/daniela.s.cruzes">Daniela S. Cruzes</a></td>
                </tr>

                <tr>
                  <td>BALANSE&nbsp;<a href="https://www.ntnu.edu/idun" target="_blank" rel="noopener noreferrer">IDUN</a></td>
                  <td>From PhD to Professor</td>
                  <td>NFR 2019 - 2022</td>
                  <td><a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/letizia.jaccheri">Letizia Jaccheri</a>&nbsp;- <a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.no/ansatte/mara.diaconu">Mara Gabriela Diaconu</a></td>
                </tr>
            
                <tr>
                  <td><a href="https://www.cost.eu/actions/CA19122" target="_blank" rel="noopener noreferrer">COST Action</a> CA19122&nbsp;Eugain</td>
                  <td>European Network for Gender Balance in Informatics&nbsp;</td>
                  <td>COST Action 2020 - 2024</td>
                  <td>Action Chair <a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/letizia.jaccheri">Letizia Jaccheri</a></td>
                </tr>
              </tbody>
            </ActiveProjectsTable>
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
