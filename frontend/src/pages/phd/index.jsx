import React                from 'react';
import { Wrapper, Row }     from '../../components/globals';
import { citings }          from './cites';
import { ReferenceWrapper } from '../../components/reference';
import { Navigation }       from '../../components/globals/Navigation';
import { H1, CustomWidthWrapper, CustomContainer } from '../../components/globals';

export default function PhD() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/phd">phd</a></Navigation>
          <CustomContainer>
            <H1>PhD and Postdoctoral Students</H1>
            <ol reversed>
              { citings.map((cite, key) => { return <ReferenceWrapper key={key}><li>{cite.cite}</li></ReferenceWrapper> }) }
            </ol>
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}