import React from 'react';
import { H1, Wrapper, Row, CustomWidthWrapper, ImageWrapper } from '../../components/globals';
import group from './static/group.jpg'; 

export default function Header() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>      
          <H1>Software For A Better Society</H1>
          <p>
            Currently, software pervades all aspects of society, environment, and human life.  The goal of our research is to establish new knowledge about opportunities and challenges posed by the rapidly accelerating pace of technological advances and how they impact the economic, political, environmental, social and technological aspects of society. 
            Our research focuses mainly on UN Goals 5 Gender and UN Goal 3 Health. 
          </p>  
          <ImageWrapper><img src={group} alt="Group" loading='eager'/></ImageWrapper>  
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}