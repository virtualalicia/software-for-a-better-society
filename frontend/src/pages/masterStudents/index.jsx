import React         from 'react';
import { Navigation} from '../../components/globals/Navigation';
import { Wrapper, Row, H1, CustomWidthWrapper, ImageWrapper, CustomContainer }           from '../../components/globals';
import { MasterSetup, Spring22, Autumn21, Autumn20, Master19, Autumn18, Master18, Autumn17, Spring21 } from './elements';
import students from './static/students.webp';

export default function Master() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/master">master</a></Navigation>
          <CustomContainer>
            <H1>Master Students</H1>
            <ImageWrapper><img src={students} alt="group"/></ImageWrapper>
            {MasterSetup}{Spring22}{Autumn21}{Spring21}{Autumn20}{Master19}{Autumn18}{Master18}{Autumn17}
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
