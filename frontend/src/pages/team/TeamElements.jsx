import React    from 'react';
import { Members, PersonContainer, Person, SocialMedia } from './elements';
import Google   from './static/Google.svg';
import Twitter  from './static/Twitter.svg';
import Facebook from './static/Facebook.svg';
import Mail     from './static/Mail.svg';
import Linkedin from './static/Linkedin.svg';
import Scopus   from './static/Scopus.svg';
import Gate     from './static/Gate.svg';


export const TeamMembers = (
  <>
    <Members><h4>Principal investigator</h4>
      <PersonContainer>
        <Person>
          <h5>Letizia Jaccheri</h5>
          <SocialMedia>
            <small><a href="mailto:letizia.jaccheri@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.no/citations?user=xBnjjysAAAAJ&hl=en&oi=ao"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/letizia/?originalSubdomain=no"><img src={Linkedin} alt="Linkedin"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/letizia.jaccheri"><img src={Facebook} alt="Facebook"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://twitter.com/letiziajaccheri"><img src={Twitter} alt="Twitter"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>


    <Members><h4>Post-doctoral students </h4>
      <PersonContainer>
        <Person>
          <h5>Alicia Julia Wilson Takaoka</h5>
          <small>Keywords: informatics, intersectionality, marginalized communities, gender equity, inclusion, digital tools, human computer interaction, climate neutrality</small>
          <SocialMedia>
            <small><a href="mailto:alicia.j.w.takaoka@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.com/citations?user=NL7BytYAAAAJ&hl=en&oi=ao"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/aliciatakaoka/"><img src={Linkedin} alt="Linkedin"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.scopus.com/authid/detail.uri?authorId=57210191279"><img src={Scopus} alt="Scopus" style={{width: '40px'}}/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.researchgate.net/profile/Alicia-Takaoka"><img src={Gate} alt="Gate"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Anna Szlavi</h5>
          <small>Postdoctoral Researcher, keywords: gender balance, inclusion, intersectionality in computer science, projects: COST Action 19122 EUGAIN - European Network for Gender Balance in Informatics, Erasmus+ Women STEM UP</small>
          <SocialMedia>
            <small><a href="mailto:anna.szlavi@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.com/citations?user=X7W8snQAAAAJ&hl=en"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/annaszlavi"><img src={Linkedin} alt="Linkedin"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.researchgate.net/profile/Anna-Szlavi"><img src={Gate} alt="Gate"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Jose David Paton (Guest)</h5>
          <small>Governance, Management, IT Processes, Sustainability (Environmental & Social), Green IT, Gender Equality</small>
          <SocialMedia>
            <small><a href="mailto:jose.d.p.romero@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.es/citations?hl=en&user=jgfa_aMAAAAJ"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/jdavidpaton"><img src={Linkedin} alt="Linkedin"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.scopus.com/authid/detail.uri?authorId=57192079711"><img src={Scopus} alt="Scopus" style={{width: '40px'}}/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.scopus.com/authid/detail.uri?authorId=57192079711"><img src={Gate} alt="Gate"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>


    <Members><h4>PhD students</h4>
      <PersonContainer>
        <Person>
          <h5>Marte Hoff Hagen</h5>
          <small>Designing Digital Rehabilitation Technology for Survivors of Childhood Critical Illness</small>
          <SocialMedia>
            <small><a href="mailto:marte.h.hagen@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.com/citations?user=SVcb24wAAAAJ&hl=no"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/martehoffhagen/"><img src={Linkedin} alt="Linkedin"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Farzana Quayyum</h5>
          <small>Cybersecurity awareness for children</small>
          <SocialMedia>
            <small><a href="mailto:farzana.quayyum@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.com/citations?user=6CaTMqQAAAAJ&hl=en"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/farzana-quayyum-058461a2/"><img src={Linkedin} alt="Linkedin"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Leif Erik Opland</h5>
          <small>Employee-driven digital innovation</small>
          <SocialMedia>
            <small><a href="mailto:leif.e.opland@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/leif-erik-opland-611248b0/"><img src={Linkedin} alt="Linkedin"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Ibrahim El Shemy</h5>
          <small>Augmented Reality for Language Learning in Children with Autism Spectrum Disorder</small>
          <SocialMedia>
            <small><a href="mailto:ibrahim.elshemy@mail.polimi.it"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://it.linkedin.com/in/ibrahim-el-shemy-5a3582172"><img src={Linkedin} alt="Linkedin"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.researchgate.net/profile/Ibrahim-El-Shemy"><img src={Gate} alt="Gate"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Orges Cico</h5>
          <small>Lean Startup and Software Engineering Education</small>
          <SocialMedia>
            <small><a href="mailto:orges.cico@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Claudia Maria Cutrupi</h5>
          <SocialMedia>
            <small><a href="mailto:claudia.m.cutrupi@ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://scholar.google.com/citations?user=baavJ0AAAAAJ&hl=it"><img src={Google} alt="GoogleScholar"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/claudiamariacutrupi/"><img src={Linkedin} alt="Linkedin"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>

    <Members><h4>Master students</h4>
      <PersonContainer>
        <Person>
          <h5>Sandra Helen Husnes</h5>
          <small>Design and Develop Digital support for inclusion</small>
          <SocialMedia>
            <small><a href="mailto:sandrahh@stud.ntnu.no"><img src={Mail} alt="Mail"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Marit Fredrikke Hansen</h5>
          <small>Design and Develop Digital support for inclusion</small>
          <SocialMedia>
            <small><a href="mailto:maritfha@stud.ntnu.no"><img src={Mail} alt="Mail"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Karen Dahl Aarhus</h5>
          <small>Investigate Digital support for inclusive, beautiful, climate neutral cities</small>
          <SocialMedia>
            <small><a href="mailto:karendaa@stud.ntnu.no"><img src={Mail} alt="Mail"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Ferdinand Ward Ådlandsvik</h5>
          <small>Investigate Digital support for inclusive, beautiful, climate neutral cities</small>
          <SocialMedia>
            <small><a href="mailto:ferdinwa@stud.ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/ferdinand-ward-%C3%A5dlandsvik-5a8656192/"><img src={Linkedin} alt="Linkedin"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Eivind Syrdalen Dovland</h5>
          <small>Investigate Digital support for inclusive, beautiful, climate neutral cities</small>
          <SocialMedia>
            <small><a href="mailto:eivindsd@stud.ntnu.no"><img src={Mail} alt="Mail"/></a></small>
            <small><a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/eivind-syrdalen-dovland-954616173/"><img src={Linkedin} alt="Linkedin"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Hanne Kyllo Ødegård</h5>
          <small>Investigating Gender and Diversity in Software Development</small>
          <SocialMedia>
            <small><a href="mailto:hannekod@stud.ntnu.no"><img src={Mail} alt="Mail"/></a></small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>
  </>
)
