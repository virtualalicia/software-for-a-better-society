import styled from 'styled-components'

export const App = styled.div`
  display: grid;
  justify-items: center;
  padding: 0 1.2rem;
`

export const Wrapper = styled.div`
  width: 100%;
  margin: ${props => props.margin || '2rem auto 3rem auto'};
`

export const Container = styled.div`
  width: 100% !important;
  max-width: 990px;
`

export const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  --bs-gutter-x: 1.5rem;
  --bs-gutter-y: 0 rem;
  margin: 0;
  align-items: center;
  justify-content: ${props => props.Justify || 'center'};
  width: 100%;
  box-sizing: border-box;
`

export const CustomWidthWrapper = styled.div`
  position: relative;
  width: 100%;
  padding: 0 !important;
  margin: auto;
  max-width: ${props => props.MaxWidth || '800px'};
  & p {
    text-align: justify;
  }
`

export const H1 = styled.h1`
  margin-bottom: 2rem;
  width: 100%;
`

export const H4 = styled.h4`
  margin-bottom: 0.5rem;
  width: 100%;
`

export const Link = styled.a`
  display: flex;
  align-items: center;
  justify-content: center;
  width: fit-content;
  margin: 1rem 0;
  text-decoration: none;
  color: inherit;
  &:hover { color: inherit; }
  & img { margin: 4px; }
`

export const Button = styled.button`
  cursor: pointer;
  width: fit-content;
  background-color: black;
  color: white;
  border: none;
  padding: 8px 16px;
  margin: 1rem 0;
  font-weight: 500;
`

export const ImageWrapper = styled(CustomWidthWrapper)`
  margin: 1rem auto;
  & img {
    width: 100%;
    height: auto;
    object-fit: cover;
  }
`

export const CustomContainer = styled(CustomWidthWrapper)`
  padding: 0 !important;
  & a {
    text-decoration: none;
    color: #305fea;
  }
  & li {
    margin-bottom: 8px;
  }
`

export const Subheading = styled.h3`
  margin: 1rem 0 0.5rem 0;
`

// Scroll back to top button
export const ScrollToTop = styled.button`
  display: block;
  position: fixed;
  bottom: 10%;
  right: 7%;
  z-index: 99;
  font-size: calc(1rem + 2px);
  border: none;
  outline: none;
  background-color: #000;
  color: white;
  cursor: pointer;
  padding: 15px;
  opacity: 0;
  pointer-events: none;
  &:hover {
    background-color: #555;
  }
`
